class Machine {
    constructor () {
        this.isTurnOn = false;
        this.turnOn = function () {
            console.log('Машина включена');
            this.isTurnOn = true;
        };
        this.turnOff = function () {
            console.log('Машина выключина');
            this.isTurnOn = false;
        };
    };
}
class HomeAppliance extends Machine {
    constructor() {
        super();
        this.isPlugIn = false;
        this.plugIn = function () {
            console.log('Подключина к сети');
            this.isPlugIn = true;
        };
        this.plugOff = function () {
            console.log('Отключина от сети');
            this.isPlugIn = false;
        };
        this.turnOn = function () {
            if (this.isPlugIn) {
                console.log('Машина включина');
            }
        };
    };
}
class WashingMachine extends HomeAppliance {
    constructor() {
        super();
        this.run = function () {
            if (this.isTurnOn && this.isPlugIn) {
                console.log('Машина работает');
            } else {
                console.log('Машина не запущена');
            }
        };
    };
}
class LightSource extends HomeAppliance {
    constructor() {
        super();
        this.level = 0;
        this.setLevel = function (level) {
            if (this.isPlugIn && (level > 0 && level <= 100)) {
                this.level = level;
                console.log('Уровень освещенности' + level);
            } else {
                console.log('Введено не коректное значение освещенности, либо прибор не включен в сеть');
            }
        };
    };
}
class AutoVehicle extends Machine {
    constructor() {
        super();
        this.x = 0;
        this.y = 0;
        this.setPosition = function (x, y) {
            this.x = x;
            this.y = y;
            console.log('Текущие координаты ' + this.x + ' ' + this.y);
        };
    };
}
class Car extends AutoVehicle {
    constructor() {
        super();
        self = this;
        this.speed = 10;
        this.setSpeed = function (speed) {
            this.speed = speed;
        };
        this.run = function (x, y) {
            var timerId = setInterval( () => {
                if (x > 0) {
                    var numX = self.x + self.speed;
                    if (numX >= x) {
                        numX = x;
                    }
                } else {
                    var numX = self.x - self.speed;
                    if (numX <= x) {
                        numX = x;
                    }
                }
                ;
                if (y > 0) {
                    var numY = self.y + self.speed;
                    if (numY >= y) {
                        numY = y;
                    }
                } else {
                    var numY = self.y - self.speed;
                    if (numY <= y) {
                        numY = y;
                    }
                }
                ;
                self.setPosition(numX, numY);
                if (numX === x && numY === y) {
                    clearInterval(timerId);
                    console.log('Машина в заданной точке');
                }
            }, 1000);
        }
    };
}